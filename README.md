# hackwork-dist
HackWork dist (Ubuntu Server based)

git clone
============

> git clone https://github.com/hackwork-tymm0/hackwork-dist %folder%

## Добавлено в дистрибутив ##
- bootloader
- mc
- htop
- lxde

Установка дистрибутива вручную
============

### Перед этим установите Ubuntu Server 14.04.4 на ваше железо ###

Перейдите в режим суперпользователя

> sudo su

Подключаем Wi-Fi

Открываем файл настроек

> nano /etc/network/interfaces

Прописываем следующие строки (выделены жирным)

--------------------------------------------------

auto wlan0

iface wlan0 inet dhcp

wpa-driver wext

wpa-ssid **<имяВашейТочкиДоступа>**

wpa-ap-scan 1

wpa-proto RSN

wpa-pairwise CCMP

wpa-group CCMP

wpa-key-mgmt WPA-PSK
	
wpa-psk **<вашКлючОтТочкиДоступа>**

--------------------------------------------------
Перезагружаем сеть

> /etc/init.d/networking restart

Перейдите в домашнюю директорию

> cd /home/USER/

Установите git

> apt-get install git

Сделайте git clone

> git clone https://github.com/hackwork-tymm0/hackwork-dist %folder%

Перейдите в папку с локальным репозиторием

> cd %folder%

Откройте папку bootloader и перенесите startsystem.sh в /bin/

> cd bootloader/
> cp startsystem.sh /bin/startsystem.sh

Перейдите в папку с startsystem.sh

> cd /bin/

Ставим startsystem.sh в автозапуск

> update-rc.d startsystem.sh defaults

Переходим обратно в локальный репозиторий

> cd /home/USER/%folder%/

Переходим в папку с программами

> cd programms/

Устанавливаем htop

> dpkg -i htop.deb

Устанавливаем mc

> dpkg -i mc.deb

Устанавливаем LXDE

> apt-get install lxde

Перезагружаемся

> shutdown -r

## Система готова к работе! ##