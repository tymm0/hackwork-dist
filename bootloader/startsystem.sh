#!/bin/bash

#
#	HackWork Dist
#
#		Based Ubuntu Server
#
# --------------------------------
#
#	Create tymm0 and Call1asS
#

echo "  ___ ___    _____           ._.    __  __      _________          ._.    __"
echo " /   |   \  /  |  |   ____   | |   / / /  \    /  \   _  \_______  | |   / / "
echo "/    ~    \/   |  |__/ ___\  |_|  / /  \   \/\/   /  /_\  \_  __ \ |_|  / / "
echo "\    Y    /    ^   /\  \___  |-|  \ \   \        /\  \_/   \  | \/ |-|  \ \  "
echo " \___|_  /\____   |  \___  > | |   \_\   \__/\  /  \_____  /__|    | |   \_\ "
echo "       \/      |__|      \/  |_|              \/         \/        |_|    "

echo "HackWork dist"
echo ""
echo "	Ubuntu Server 14.04.4 LTS"
echo ""
echo "#########################################"
echo ""
echo ""
echo ""
echo "		Choose a boot method:"
echo ""
echo "	[1] mc"
echo "	[2] htop"
echo "	[3] lxde"
echo "	[4] SSH connection"
echo "	[5] shell"
echo ""
echo "Choose: "
read pick

if [[ $pick = '1' ]]; then # Запускает Midnight Commander (для подключения к SFTP)
	mc
fi

if [[ $pick = '2' ]]; then # Запускает диспетчер задач htop
	htop
fi

if [[ $pick = '3' ]]; then # Запускает графическую среду LXDE
	startlxde
fi

if [[ $pick = '4' ]]; then # Запускает соединение по SSH
	LOGINBOOL=0 	#
	HOSTBOOL=0 	# Булы для проверки валидности вводимых данных
	PORTBOOL=0 	#

	clear

	echo ""
	echo "################ SSH ################"
	echo ""
	echo "Choose login: "
	read loginSSH
	echo "Choose host:"
	read hostSSH
	echo "Choose port:"
	read portSSH
	clear
	echo "Building connection query..."
	echo ""

	if [[ $loginSSH != '' ]]; then	# Проверка логина
		echo "Checking login...			[OK]"
		LOGINBOOL=1
	else
		echo "Checking login...			[FAIL]"
	fi

	if [[ $hostSSH != '' ]]; then	# Проверка хоста
		echo "Checking host...			[OK]"
		HOSTBOOL=1
	else
		echo "Checking host...			[FAIL]"
	fi

	if [[ $portSSH != '' ]]; then	# Проверка порта
		echo "Checking port...			[OK]"
		PORTBOOL=1
	else
		echo "Checking port...			[FAIL]"
	fi

	# Проверка значения булов #

		loginQuery=0

		if [[ $LOGINBOOL = 1 ]]; then
			echo "Checking LOGINBOOL			[TRUE]"

			if [[ $HOSTBOOL = 1 ]]; then
				echo "Checking HOSTBOOL			[TRUE]"

				if [[ $PORTBOOL = 1 ]]; then
					echo "Checking PORTBOOL			[TRUE]"
					echo "SSH connect..."

					loginQuery="${loginSSH}@${hostSSH} -p ${portSSH}"

					ssh $loginQuery
				else
					echo "Checking HOSTBOOL		[FALSE]"
					echo "## ENDING CONNECTION ##"
					echo ""
					exit
				fi
			else
				echo "Checking HOSTBOOL		[FALSE]"
				echo "## ENDING CONNECTION ##"
				echo ""
				exit
			fi 
		else
			echo "Checking LOGINBOOL		[FALSE]"
			echo "## ENDING CONNECTION ##"
			echo ""
			exit
		fi

	# /Проверка значения булов #
fi

if [[ $pick = '5' ]]; then
	clear
	exit
fi